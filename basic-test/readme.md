<center>
<img src="https://cdn.website-editor.net/7faf6d1ccff4459495853794e59fe9be/dms3rep/multi/mobile/Befeni_ohne_Claim.png" height="100">

# Befeni Technical Test [Basic]
</center>

## Requirements
* NodeJS

## How to run
Assuming you are already cloned to your local, use your favorite command-line to run follow command for install necessary dependencie:
```
cd befeni-test-master/basic-test/
npm i
```
After install completed,
If you want to run only a program use:
```
npm run start
```
If you want to run as web server use:
```
npm run serve
```