const fs = require("fs")
const express = require("express")
const app = express()

const PORT = 3000
const INST_SEPERATOR = " "
const LINE_SEPERATOR = "\n"

const doCalculate = (lineArray) => {
  const numArray = []
  const opArray = []
  lineArray.forEach((line) => {
    const temp = line.split(INST_SEPERATOR)
    switch (temp[0]) {
      case "add":
      case "divide":
      case "subtract":
      case "multiply":
        opArray.push(temp[0])
        numArray.push(Number(temp[1]))
        break
      case "apply":
        numArray.unshift(Number(temp[1]))
        break
      default:
        break
    }
  })
  return numArray.reduce((prevVal, currVal, idx) => {
    if (idx == 0) {
      return currVal
    }
    switch (opArray[idx - 1]) {
      case "add":
        return prevVal + currVal
      case "divide":
        return prevVal / currVal
      case "subtract":
        return prevVal - currVal
      case "multiply":
        return prevVal * currVal
      default:
        break
    }
  })
}

const main = () => {
  const testExamPath = "files/"
  const files = fs
    .readdirSync(testExamPath)
    .filter((file) => fs.lstatSync(testExamPath + file).isFile())
  files.forEach((file) => {
    const fileData = fs.readFileSync(testExamPath + file, "utf8")
    const lines = fileData.split(LINE_SEPERATOR)
    console.log(doCalculate(lines))
  })
}

const serve = () => {
  app.get("/", (req, res) => {
    res.sendFile("./web_frontend.html", { root: __dirname })
  })

  app.post("/doCalculate", express.json(), (req, res) => {
    try {
      const { dataArray } = req.body
      if (!dataArray) {
        throw Error("No Array!")
      }
      if (dataArray.length < 1) {
        throw Error("No Element Inside!")
      }

      res.json({
        success: true,
        result: doCalculate(dataArray)
      })
    } catch (err) {
      res.json({
        success: false,
        error: err.message
      })
    }
  })

  app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`)
  })
}

const appArgs = process.argv
switch (appArgs[2]) {
  case "server":
    serve()
    break
  default:
    main()
}
